/**
 * @file
 * Custom JS for the Webform Same Values module.
 */

(function ($) {
  'use strict';

  /**
   * Save the webform values into a cookie for later.
   */
  Drupal.behaviors.webformSameValuesSave = {
    attach: function (context, settings) {
      $('form.webform-client-form').on('submit', function() {
        var form_id = $(this).attr('id');
        var submitted_values = {};

        // Combine all of the form values.
        $('input.form-text, select, text', this).each(function() {
          submitted_values[this.name] = $(this).val();
        });

        // Make the data save to store in a cookie.
        submitted_values = JSON.stringify(submitted_values);
        submitted_values = btoa(submitted_values);

        // Save the values to a cookie.
        $.cookie(form_id, submitted_values);
      });
    }
  };

  /**
   * Load any values that might have been saved before.
   */
  Drupal.behaviors.webformSameValuesLoad = {
    attach: function (context, settings) {
      $('form.webform-client-form').each(function() {
        var form_id = $(this).attr('id');

        // Check to see if any values were saved previously.
        var saved_values = $.cookie(form_id);

        if (saved_values === null) {
          return;
        }

        // Extract the saved data.
        saved_values = atob(saved_values);
        saved_values = JSON.parse(saved_values);

        // Track whether values are actually updated.
        var form_changed = false;

        // Combine all of the form values.
        $('input.form-text, select, textarea', this).each(function() {
          if (typeof saved_values[this.name] === 'undefined') {
            return;
          }
          $(this).val(saved_values[this.name]);

          // The form was changed.
          form_changed = true;
        });

        // If values were loaded from cookies, auto-submit the form.
        $('#' + form_id).submit();
      });
    }
  };

})(jQuery);
