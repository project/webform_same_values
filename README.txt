Webform Same Values
-------------------
Stores webform values in a cookie, so the values can be filled in again if the
form is displayed a second time.

No configuration is currently available, once the module is enabled it will work
automatically.

Works quite well with the Webform File Gateway module:
* https://www.drupal.org/project/webform_file_gateway


Credits / contact
--------------------------------------------------------------------------------
Created by Damien McKenna [1].

The best way to contact the author is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
  https://www.drupal.org/project/issues/webform_same_values


References
--------------------------------------------------------------------------------
1: https://www.drupal.org/u/damienmckenna
